// GET REQUEST

//There are 2 ways for us to achieve an axios get request, either you go for the long code, or short code. The idea here is that we need to know the method, url, and params are optional.
//After defining axios, since axios is a promise, we need to create a then and catch code.
function getTodos() {
  console.log('GET Request');

  //Long Version
  // axios({
  //   method:'get',
  //   url: 'https://jsonplaceholder.typicode.com/todos',
  //   params:{
  //     _limit: 3
  //   }
  // })
  //   .then(res => showOutput(res))
  //   .catch(err => console.error(err))

  axios
    .get('https://jsonplaceholder.typicode.com/todos?_limit=5')
    .then(res => showOutput(res))
    .catch(err => console.error(err))
}

// POST REQUEST
function addTodo() {
  console.log('POST Request');

  // Long Version
  // axios({
  //   method: 'post',
  //   url: 'https://jsonplaceholder.typicode.com/todos',
  //   data:{
  //     title: 'New Todo',
  //     completed: false
  //   }
  // })
  //   .then(res => showOutput(res))
  //   .catch(err => console.error(err))

  // Short Version
  axios.post('https://jsonplaceholder.typicode.com/todos',{title:"New Todo", completed: false})
    .then(res => showOutput(res))
    .catch(err => console.error(err))
}

// PUT/PATCH REQUEST
function updateTodo() {
  console.log('PUT/PATCH Request');
  //The difference betweenn put and patch:
  // PUT: Replace the entire resource
  // Patch: Update incrementally

  // Long Version (PUT)
  // axios({
  //   method: 'put',
  //   url: 'https://jsonplaceholder.typicode.com/todos/1',
  //   data:{
  //     title: 'Update Todo',
  //     completed: true
  //   }
  // })
  //   .then(res => showOutput(res))
  //   .catch(err => console.error(err))

  axios.put('https://jsonplaceholder.typicode.com/todos/1',{title:"New Todo", completed: true})
    .then(res => showOutput(res))
    .catch(err => console.error(err))

  // Short Version (PATCH)
  // axios.put('https://jsonplaceholder.typicode.com/todos/1',{title:"New Todo", completed: true})
  //   .then(res => showOutput(res))
  //   .catch(err => console.error(err))

}

// DELETE REQUEST
function removeTodo() {
  console.log('DELETE Request');

  axios.delete('https://jsonplaceholder.typicode.com/todos/1',{title:"New Todo", completed: true})
    .then(res => showOutput(res))
    .catch(err => console.error(err))
}

// SIMULTANEOUS DATA
function getData() {
  console.log('Simultaneous Request');
}

// CUSTOM HEADERS
function customHeaders() {
  console.log('Custom Headers');
}

// TRANSFORMING REQUESTS & RESPONSES
function transformResponse() {
  console.log('Transform Response');
}

// ERROR HANDLING
function errorHandling() {
  console.log('Error Handling');
}

// CANCEL TOKEN
function cancelToken() {
  console.log('Cancel Token');
}

// INTERCEPTING REQUESTS & RESPONSES

// AXIOS INSTANCES

// Show output in browser
function showOutput(res) {
  document.getElementById('res').innerHTML = `
  <div class="card card-body mb-4">
    <h5>Status: ${res.status}</h5>
  </div>
  <div class="card mt-3">
    <div class="card-header">
      Headers
    </div>
    <div class="card-body">
      <pre>${JSON.stringify(res.headers, null, 2)}</pre>
    </div>
  </div>
  <div class="card mt-3">
    <div class="card-header">
      Data
    </div>
    <div class="card-body">
      <pre>${JSON.stringify(res.data, null, 2)}</pre>
    </div>
  </div>
  <div class="card mt-3">
    <div class="card-header">
      Config
    </div>
    <div class="card-body">
      <pre>${JSON.stringify(res.config, null, 2)}</pre>
    </div>
  </div>
`;
}

// Event listeners
document.getElementById('get').addEventListener('click', getTodos);
document.getElementById('post').addEventListener('click', addTodo);
document.getElementById('update').addEventListener('click', updateTodo);
document.getElementById('delete').addEventListener('click', removeTodo);
document.getElementById('sim').addEventListener('click', getData);
document.getElementById('headers').addEventListener('click', customHeaders);
document
  .getElementById('transform')
  .addEventListener('click', transformResponse);
document.getElementById('error').addEventListener('click', errorHandling);
document.getElementById('cancel').addEventListener('click', cancelToken);